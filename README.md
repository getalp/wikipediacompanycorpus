# The Wikipedia company corpus

This repo contains a corpus of ~51K companies and the scripts used to extract them as used in the INLG 2018 paper: *Generation of Company descriptions using concept-to-text and text-to-text deep models: dataset collection and systems evaluation*. please check the paper for further information about the corpus.


## 1) Download the processed corpus

You can download the train, dev and test sets [here](https://gricad-gitlab.univ-grenoble-alpes.fr/getalp/wikipediacompanycorpus/tree/master/download_corpus/wikipedia_companies.zip).

### Details of the corpus
* The compnay names were all extracted from the wikipedia dump `enwiki-20170820-pages-articles.xml.bz2`. Check details given [here](https://gricad-gitlab.univ-grenoble-alpes.fr/getalp/wikipediacompanycorpus/tree/master/wikipedia_dump) to download it.


* For each company, we extracted the `company name`, `abstract`, `infobox` and the `body text`. Please not that some companies do not have body text. These companies can be identified when "no_body" token is seen in their corresponding body text field.

* The abstract, infobox and body text can also be downloaded in seperate text files [here](https://gricad-gitlab.univ-grenoble-alpes.fr/getalp/wikipediacompanycorpus/tree/master/download_corpus).



## 2) Extract and process the corpus yourself
This will give you the chance to change the scripts and process the corpus in your own way. See [scripts](https://gricad-gitlab.univ-grenoble-alpes.fr/getalp/wikipediacompanycorpus/tree/master/scripts) for more details.




Please cite the following paper if you use our data for your own research:

```
@InProceedings{W18-6532,
  author = 	"Qader, Raheel and Jneid, Khoder and Portet, Fran{\c{c}}ois and Labb{\'e}, Cyril",
  title = 	"Generation of Company descriptions using concept-to-text and text-to-text deep models: dataset collection and systems evaluation",
  booktitle = 	"Proceedings of the 11th International Conference on Natural Language Generation",
  year = 	"2018",
  publisher = 	"Association for Computational Linguistics",
  pages = 	"254--263",
  location = 	"Tilburg University, The Netherlands",
  url = 	"http://aclweb.org/anthology/W18-6532"
}
```
