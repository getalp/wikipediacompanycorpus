# Scripts to recreate the corpus

This folder contains the scripts used to extract ~51K **company names** along with their **abstracts**, **infoboxes** and **content text** from Wikipedia.

## Prerequisites
Install packages located in requirements.txt
```
pip install -r requirements.txt
```

## Running the scripts
1) Run `extract_company_names.py` to extract all company names from a wikipedia dump. Note that some of the companies were extracted manually from https://en.wikipedia.org/wiki/Lists_of_companies. So the total number of companies obtained will be less than 51K.

2) Run `filter_company_names.py` to filter out companies with no or less than two infobox attibutes (which are probably not companies).

3) Run `extract_infobox.py` to extract infoboxes for each company. Two versions of the infobox are extracted: inforbox1 and infobox2. The first one contains the text version of the infobox attributes, e.g.  Headquarters": "London, United Kingdom", while infobox2 contains the listed version of the attibutes, e.g.  "Headquarters": ["London",", ","United Kingdom"]. Both version will later be used to extract a clean version of the infobox attributes.

4) Run `extract_abstracts.py` to extract abstract of each company using the wikipedia api.

5) Run `extract_body.py` to extract body text of each company using the wikipedia api.

6) Run `merge_abstract_body_infobox` to merge company names, their infoboxes abstracts, and article bodies in a json file.

7) Finally run `create_dataset.py` to clean the data and create the final dataset (.csv) with two columns: mr (meaning representations) containing infobox attributes, and ref (reference) containing the abstracts.


## Scripts
extract_company_names.py **dump_file** 
 ```
arguments:
		dump_file   path to wikipedia dump file
```

filter_company_names.py **input_file** **output_file**
 ```
arguments:
	input_file   path to file containing company names (text file)
	output_file  path to store the output (text file)
```

extract_infobox.py **input_file** **infobox1** **infobox2**
 ```
arguments:
	input_file  path to file containing company names (.json)
	infobox1    path to store infobox1 (.json)
	infobox2    path to store infobox2 (.json)
  ```


extract_abstracts.py **input_file** **abstract_file**
 ```
arguments:
	input_file     path to file containing company names (text file)
	abstract_file  path to store abstracts (.json)
  ```


extract_body.py **input_file** **output_dir**
 ```
arguments:
	input_file     path to file containing company names (text file)
	output_dir     path of directory to store bodies files
  ```


merge_abstract_body_infobox.py **abstract_file** **infobox1_file** **infobox2_file** **body_file** **output_file**
 ```
arguments:
	abstract_file  		path to file containing abstracts (.json)
	infobox1_file       path to file containing infobox1 (.json)
	infobox2_file       path to file containing infobox2 (.json)
	body_file       	path to file containing body file (.json)
	output         		path to store the merged file (.json)
  ```


create_dataset.py **input_file** **output_file**
 ```
arguments:
	input_file   path to file containing abstracts and infoboxes (.json)
	output_file  path of file to store the dataset (.csv)
  ```
