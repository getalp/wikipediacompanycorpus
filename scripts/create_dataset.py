# -*- coding: utf-8 -*-


import re
import codecs
import json
import sys, os
import numpy as np
import random
from matplotlib import pyplot as plt
from collections import *
import nltk
import string
import difflib
from nltk.corpus import stopwords
import argparse

import spacy
#python -m spacy download en
nlp = spacy.load('en')




stop_words_list = set(stopwords.words('english'))

allowed_features = ['Name', 'Headquarters','Founded','Industry','Type','Key people','Products','Number of employees','Founders', 'Founder', 'Owner','Defunct','Services']
features_similarity = ['Headquarters', 'Type', 'Number of employees']



MAX_SENT_LEN = 1000 # max abstract length
MIN_SENT_LEN = 0 # min abstract length
MAX_NB_PER_FEATURE = 3 # e.g. Product1[...], Product2[...], Product3[...], Product4[...], Product5[...]
MIN_NB_FEATURE = 2 # minimum number of features per abstract







number_pattern = r'([+-~]?(?:(?:\d+(?:(\s+)?[\-\.\,](\s+)?\d*)?)|(?:\.\d+))\+?)'
month_pattern = '(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)'
year_pattern = '(\d{4})'
month_year_pattern = '(' + month_pattern + '\s+?' + year_pattern + ')'
garbage = ['\"','\'','\\n','\;', 'and',' ', '/', '\[\d+\]']
combine_garbage = '|'.join(['^(' + exp + ')$' for exp in garbage])
split_expressions = "\band\b|,"
nb_location_pattern = '(' + number_pattern + '\s+' + '(countries|franchised|factories|plants|stores|chains|offices|locations|countries|restaurants|hotels|warehouses|countries|branches|total)' + ')'
currency_pattern = r'(\d+(\s+)?(\,|to \d+)?(\s+)?(Billion|billion|Million|million|Trillion|trillion|M|B|T|m|b|t)\b)'



punctuations = set(string.punctuation)

def strip_punctuation(text, exl=None):
	punctuations = set(string.punctuation)

	if exl: punctuations = [item for item in punctuations if item not in exl]

	return ''.join(ch for ch in text.strip() if ch not in punctuations)





def parse_founded(feature):

	match = re.search(r'\d{4}', feature)

	if match: return [match.group()]
	else: return None




def parse_number_of_employees(feature):
	
	match = re.search(number_pattern, feature)

	if match: return [match.group()]
	else: return [feature]



def parse_industry(feature, detailed_feature):
	
	clean_features = [feat for feat in detailed_feature if not re.match(combine_garbage, feat) and feat]
	clean_features = [strip_punctuation(feat) for feat_item in clean_features for feat in re.split(split_expressions, feat_item) if feat]
	clean_features = [feat for feat in  clean_features if feat]
	return clean_features




def remove_month_year(text):

	combine_month_year = '(\('+year_pattern+'\))|(\('+month_pattern+'(\s+)?'+year_pattern+'\))'
	clean_text = re.sub(combine_month_year, '', text)
	return clean_text.strip()



def extract_nb_location(text):

	match = re.search(nb_location_pattern, text)

	if match: return match.group()
	else: return  text



def parse_number_of_locations(feature, detailed_feature):

	clean_features = [remove_month_year(feat) for feat in detailed_feature]
	clean_features = [strip_punctuation(feat) for feat in clean_features if not re.match(combine_garbage, feat)]

	clean_features = ', '.join([feat for feat in clean_features if feat])
	clean_features = extract_nb_location(clean_features)

	return [clean_features]




def parse_headquarters(feature, detailed_feature):

	clean_features = [strip_punctuation(feat) for feat in detailed_feature if not re.match(combine_garbage, feat)]
	clean_features = ', '.join([feat for feat in clean_features if feat])
	return [clean_features]






# .encode("ascii", "ignore")
def parse_revenue(feature, detailed_feature):
	clean_features = [remove_month_year(feat) for feat in detailed_feature]
	clean_features = [strip_punctuation(feat) for feat in clean_features if not re.match(combine_garbage, feat)]
	clean_features = ', '.join([feat for feat in clean_features if feat])
	clean_features= str(clean_features.encode("ascii", "ignore"))
	match = re.search(currency_pattern, clean_features)

	if match: return [match.group()]
	else: return [clean_features]




def parse_defunct(feature):
	combine_month_year = '(\(?'+year_pattern+'\)?)|(\(?'+month_pattern+'(\s+)?'+year_pattern+'\)?)'

	match = re.search(combine_month_year, feature)

	if match:return [match.group()]
	else: return  [feature]



def parse_services(feature, detailed_feature):
	

	clean_features = [feat for feat in  re.split(' and |,|\\n', feature)]
	clean_features = [feat for feat in clean_features if not re.match(combine_garbage, feat)]
	clean_features = [strip_punctuation(feat) for feat_item in clean_features for feat in re.split(',', feat_item) if feat]
	clean_features = [re.sub('^and', '', feat) for feat in  clean_features if feat]


	return clean_features


def parse_founders(feature, detailed_feature):
	clean_features = [feat for feat in  re.split(' and |,|\\n', feature)]
	clean_features = [re.sub(combine_garbage, '', feat) for feat in clean_features if feat]

	tmp_clean_features = []
	for feat in clean_features:
		if re.match('\(.+(\s+and\s+.+)?\)', feat):
			tmp_clean_features[-1] += (' '+feat)
		elif feat.strip().lower() in ['ceo', 'president']:
			tmp_clean_features[-1] += (' ('+feat+')')			
		else:
			tmp_clean_features.append(feat)
	clean_features = [strip_punctuation(feat, ['(', ')']) for feat in tmp_clean_features if feat]

	return clean_features



def parse_products(feature, detailed_feature):
	

	clean_features = [feat for feat in  re.split(' and |,|\\n', feature)]
	clean_features = [feat for feat in clean_features if not re.match(combine_garbage, feat)]
	clean_features = [strip_punctuation(feat) for feat_item in clean_features for feat in re.split(',', feat_item) if feat]
	clean_features = [re.sub('^and', '', feat) for feat in  clean_features if feat]

	return clean_features



def parse_powner(feature, detailed_feature):
	
	clean_features = [feat for feat in detailed_feature if not re.match(combine_garbage, feat.strip())]
	clean_features = [re.sub('^and', '', feat) for feat in  clean_features if feat]
	clean_features = [re.sub(number_pattern,'', feat) for feat in clean_features if feat]
	clean_features = [strip_punctuation(feat) for feat in clean_features if feat]
	clean_features = [re.sub('–','',feat) for feat in clean_features if feat]



	clean_features = [feat for feat in clean_features if feat]

	return clean_features



def parse_key_people(feature, detailed_feature):
	
	#TODO: seperate by .+\(.+\)

	clean_features = [feat for feat in  re.split(';|\\n', feature)]



	tmp_clean_features = []

	if len(clean_features) >0:
		tmp_clean_features = [clean_features[0]]
	else: return feature

	for feat in clean_features[1:]:
		if re.match('\(.+(\s+and\s+.+)?\)', feat):
			tmp_clean_features[-1] += (' '+feat)		
		else:
			tmp_clean_features.append(feat)


	clean_features = [re.sub('\[\d+\]', '', feat) for feat in tmp_clean_features if feat]
	clean_features = [strip_punctuation(feat, ['(', ')']) for feat in clean_features if feat]



	return clean_features



def parse_others(feature):

	feature = feature.strip()
	clean_feature = re.sub('\r?\n', ', ', feature)
	return [clean_feature]



# removes infobox attributes that doesnt exist in the abstract
# cut_words: cuts words which exists inside an infobox but not in the abstract => headquarter[Street 123, New York, United states]
def similar_words(source_words, target_words, cut_words=False, cutoff=None):


	#DEBUG###############################
	# print('source_words: ', source_words)
	# print('target_words: ', target_words)
	#DEBUG###############################


	similar_words = []

	for source_word in source_words:

		source_word = source_word.strip()

		if not source_word or source_word in stop_words_list or source_word in punctuations: continue


		source_word_sim = 0

		for target_word in target_words:

			target_word = target_word.strip()

			if not target_word or target_word in stop_words_list or target_word in punctuations: continue

			sim = difflib.SequenceMatcher(None, source_word.lower(), target_word.lower()).ratio()


			if sim > source_word_sim: source_word_sim = sim


		if source_word_sim >= cutoff:
			similar_words.append(source_word)
			if not cut_words:
				return source_words
	else:
		if not cut_words:
			return None





	#DEBUG###############################
	# print('similar_words: ', similar_words)
	# input()
	#DEBUG###############################



	# if (sum(similar_words) / float(len(similar_words))) > cutoff:
	# 	return True
	# else:
	# 	return False


	return similar_words



def clean_text(inf_abs_bdy):


	clean_inf_abs_bdy = {}

	for c_i, company in enumerate(inf_abs_bdy):


		clean_inf_abs_bdy[company] = {}

		clean_inf_abs_bdy[company]['infobox'] = {}
		clean_inf_abs_bdy[company]['abstract'] = ''
		clean_inf_abs_bdy[company]['body'] = ''

		for feature in inf_abs_bdy[company]['infobox1']:

			feat_val = inf_abs_bdy[company]['infobox1'][feature]

			feat_val = re.sub(r'^b\'', '\'', feat_val)
			feat_val = re.sub(r'^b\"', '\"', feat_val)
			feat_val = re.sub(r'\"', '', feat_val)
			feat_val = re.sub(r'\'', '', feat_val)

			inf_abs_bdy[company]['infobox1'][feature] = feat_val

			stripped_feature = feature.strip().strip('\n')

			if stripped_feature == 'Founded':
				cleaned = parse_founded(inf_abs_bdy[company]['infobox1'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned
			elif stripped_feature == 'Number of employees':
				cleaned = parse_number_of_employees(inf_abs_bdy[company]['infobox1'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned
			elif stripped_feature == 'Industry':
				cleaned = parse_industry(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned
			elif stripped_feature == 'Number of locations':
				cleaned = parse_number_of_locations(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned
			elif stripped_feature == 'Headquarters':
				cleaned = parse_headquarters(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned
			elif stripped_feature == 'Revenue':
				cleaned = parse_revenue(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned
			elif stripped_feature == 'Defunct':
				cleaned = parse_defunct(inf_abs_bdy[company]['infobox1'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned
			elif stripped_feature == 'Services':
				cleaned = parse_services(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned				
			elif stripped_feature in ['Founder', 'Founders']:
				try:
					cleaned = parse_founders(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
					inf_abs_bdy[company]['infobox1'][feature] = cleaned	
				except:
					pass
			elif stripped_feature == 'Products':
				cleaned = parse_products(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned	
			elif stripped_feature == 'Owner':
				try:
					cleaned = parse_powner(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
					inf_abs_bdy[company]['infobox1'][feature] = cleaned	
				except:
					pass
			elif stripped_feature == 'Key people':
				cleaned = parse_key_people(inf_abs_bdy[company]['infobox1'][feature], inf_abs_bdy[company]['infobox2'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned
			else:
				cleaned = parse_others(inf_abs_bdy[company]['infobox1'][feature])
				inf_abs_bdy[company]['infobox1'][feature] = cleaned



			clean_inf_abs_bdy[company]['infobox'][stripped_feature] = inf_abs_bdy[company]['infobox1'][feature]



		abstract = inf_abs_bdy[company]['abstract']
		abstract = re.sub(r'\"', '', abstract)
		abstract = re.sub(r'\'', '', abstract)
		abstract = re.sub(r'\n', '', abstract)
		abstract = re.sub(r'\;', '', abstract)


		body_text = inf_abs_bdy[company]['body']
		body_text = re.sub(r'\"', '', body_text)
		body_text = re.sub(r'\'', '', body_text)
		body_text = re.sub(r'\n', '_h_ ', body_text)
		body_text = re.sub(r'\;', '', body_text)


		clean_inf_abs_bdy[company]['abstract'] = abstract
		clean_inf_abs_bdy[company]['body'] = body_text

	return clean_inf_abs_bdy






def create_dataset(inf_abs_bdy, output_dir):

	# for visualization and debuging ====
	abstract_len_feature_ratio = []
	feat_no_dist = []
	abstract_len_dist = []

	unique_feat = []
	max_abstract_len = 0
	max_feat_len = 0
	#====================================




	inf_abs_bdy = clean_text(inf_abs_bdy)


	all_infobox_string = ''
	all_abstract_string = ''
	all_body_text_string = ''



	all_company_string = 'infobox,abstract,bodytext'
	all_company_string+= '\n'


	for c_i, company in enumerate(inf_abs_bdy):


		company_string = ''


		# if not all(ord(c) < 128 for c in inf_abs_bdy[company]['abstract']): continue
		# if len(abstract) > MAX_SENT_LEN: continue


		#remove non ascii character (this should be avoided for french and other langauges)
		abstract = str(inf_abs_bdy[company]['abstract'].encode("ascii", "ignore").decode("utf-8", "ignore"))
		body_text = str(inf_abs_bdy[company]['body'].encode("ascii", "ignore").decode("utf-8", "ignore"))






		# force abstract length to be equal or less than MAX_SENT_LEN and larger than MIN_SENT_LEN========================
		abstract_spacy = nlp(abstract)
		sentences = [sent.string.strip() for sent in abstract_spacy.sents]
		trunc_sentences = sentences[0]  + ' '

		if len(sentences) > 1:

			for sentence in sentences[1:]:
				if len(trunc_sentences + sentence) < MAX_SENT_LEN:
					trunc_sentences += sentence + ' '
				else:
					break

		trunc_sentences = trunc_sentences.strip()
		if trunc_sentences[-1] != '.': trunc_sentences+='.'


		if len(trunc_sentences) > MAX_SENT_LEN or len(trunc_sentences) < MIN_SENT_LEN: continue
		if len(trunc_sentences.strip()) > max_abstract_len: max_abstract_len = len( trunc_sentences.strip())

		abstract_words = [word for word in trunc_sentences.split()]
		#==================================================================================================================




		# number of features should be larger than MIN_NB_FEATURE
		if len([feature for feature in inf_abs_bdy[company]['infobox'] if feature in allowed_features]) < MIN_NB_FEATURE: continue


		company_string+='"'
		infobox_string = ''
		abstract_string = ''
		body_text_string = ''


		inf_abs_bdy[company]['infobox']['Name'] = [company]
		

		# feature contraint: if any of these features is not contained, we discard the company 
		feat_filter = {'Name':False, 'Headquarters':False, 'Founded':False, 'Industry':False}
		

		# debugging==========
		feature_counter = 0
		total_nb_feature = 0
		#====================

		multi_features_1 = ['Key people','Founders', 'Founder', 'Owner']
		multi_features_2 = ['Industry','Products','Services']


		attr_list = defaultdict(list)

		for feature in allowed_features:

			if feature in inf_abs_bdy[company]['infobox']:

				if inf_abs_bdy[company]['infobox'][feature]:

					feat_vals = inf_abs_bdy[company]['infobox'][feature]


					for f_i, feat_val in enumerate(feat_vals):

						if f_i >= MAX_NB_PER_FEATURE: break ## UNCOMMENT THIS LINE LATER ***************************************************************

						feat_val = str(feat_val.encode("ascii", "ignore").decode("utf-8", "ignore"))

						if feature in features_similarity: 
							feat_val = similar_words(feat_val.split(), abstract_words, True, cutoff=0.7)
							feat_val = ' '.join(feat_val)

						if feat_val:

							#prevent adding same value to different atrributes => product1[automobiles], product2[automobiles], product3[automobiles]
							if feature in multi_features_1:
								if feat_val not in [inf_abs_bdy[company]['infobox'][multi_feature] for multi_feature in multi_features_1 if multi_feature in inf_abs_bdy[company]['infobox']]:
									attr_list[feature].append(feat_val)
							elif feature in multi_features_2:
								if feat_val not in [inf_abs_bdy[company]['infobox'][multi_feature] for multi_feature in multi_features_2 if multi_feature in inf_abs_bdy[company]['infobox']]:
									attr_list[feature].append(feat_val)
							else:
								attr_list[feature].append(feat_val)

									

						

		for feature1 in allowed_features:
			for feature2 in attr_list:
				if feature2 == feature1:

					for v_i, value in enumerate(attr_list[feature2]):

						infobox_string+='{}[{}], '.format(feature2+str(v_i+1), value.replace(',',''))
						unique_feat.append(feature2+str(v_i+1))

					if feature1 in feat_filter: feat_filter[feature1] = True
					feature_counter+=1


		if len(infobox_string[:-2].split(',')) < MIN_NB_FEATURE: continue





		abstract_len_feature_ratio.append([len(abstract_words), (feature_counter)])
		
		feat_no_dist.append(total_nb_feature)
		abstract_len_dist.append(len(abstract_words))


		# if ((feature_counter/len(abstract.split()))*100) < 10: continue
		# if ((feature_counter/total_nb_feature)*100) < 100: continue


		
		# if not all([feat_filter[key] for key in feat_filter]): continue


		infobox_string= infobox_string[:-2]
		if len(infobox_string) > max_feat_len: max_feat_len = len(infobox_string)


		company_string+=infobox_string

		company_string+='", '
		company_string+='"'


		# remove description between parantheses (mostly contains company names transliterations), #TODO only remove the ones in the beginning
		abstract_string = re.sub(r'\(.+?\)', '', trunc_sentences)
		if len(abstract_string) < MIN_SENT_LEN: continue

		company_string+=abstract_string
		company_string+='", '

		# body_text_string = body_text if body_text!= 'no_body' else ''
		body_text_string = body_text

		company_string+='"'
		company_string+=body_text_string
		company_string+='"'


		all_company_string+=company_string + '\n'

		if infobox_string and abstract_string and body_text_string:
			all_infobox_string+=infobox_string + '\n'
			all_abstract_string+=abstract_string + '\n'
			all_body_text_string+=body_text_string + '\n'




		print(c_i)


	dataset_file='dataset.csv'
	with open(os.path.join(output_dir,dataset_file), 'w') as f_out:
		f_out.write(all_company_string)
			


	infobox_file='infobox.txt'
	with open(os.path.join(output_dir,infobox_file), 'w') as f_out:
		f_out.write(all_infobox_string)
			

	abstract_file='abstract.txt'
	with open(os.path.join(output_dir,abstract_file), 'w') as f_out:
		f_out.write(all_abstract_string)


	body_text_file='body_text.txt'
	with open(os.path.join(output_dir,body_text_file), 'w') as f_out:
		f_out.write(all_body_text_string)



	print(set(unique_feat))

	print('max_abstract_len: ', max_abstract_len)
	print('max_feat_len: ', max_feat_len)







if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("input_file", type=str, help="path to file containing abstracts, infoboxes, and bodies (.json)")
	parser.add_argument("output_dir", type=str, help="directory  to store the dataset")

	args = parser.parse_args()


	inf_abs_bdy = json.load(open(args.input_file))

	create_dataset(inf_abs_bdy, args.output_dir)

