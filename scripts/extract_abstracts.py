import sys
import json
import requests
import argparse




def extract_abstracts(company_names_path, abstract_path):


	# load company names to be used to extract their text 
	companies = []

	with open(company_names_path) as fin:
		for line in fin:
			line = line.strip()

			companies.append(line)


	abstracts = {}


	for c_i, company in enumerate(companies):

		print (c_i)

		response = requests.get('https://en.wikipedia.org/w/api.php?action=query&prop=extracts&titles={}&exintro=&explaintext=&redirects=1&format=json'.format(company)).json()
		page = next(iter(response['query']['pages'].values()))

		if ('title' in page and 'extract' in page)  and (page['title'] and page['extract']):

			abstracts[company] = page['extract']


	with open(abstract_path, 'w') as fh:
		fh.write(json.dumps(abstracts))





if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("input_file", type=str, help="path to file containing company names (text file)")
	parser.add_argument("abstract_file", type=str, help="path to store abstracts (.json)")

	args = parser.parse_args()

	extract_abstracts(args.input_file, args.abstract_file)
