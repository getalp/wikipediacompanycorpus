# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:46:46 2018

@author: pc
"""
import requests
import xmltodict
import json
from bs4 import BeautifulSoup
import re
import nltk.data
import matplotlib.pyplot as plt
import collections
import os
import argparse

"%matplotlib qt"
'''
To read the companies and create two versions: one the initial copy and the second is the
encoded copy in order to pass it to link
'''
def process_Companies(path):
    with open(path, 'r', encoding = 'utf-8') as f:
        content = f.readlines()
    companies = [x.strip() for x in content]
    companies_encoded = [x.replace("&", "%26") for x in companies]
    companies_encoded = [x.replace("+", "%2B") for x in companies_encoded]
    return [companies, companies_encoded]

'''
this method will return an html text for the given company !pip install -U nltk
'''
def html_Company(url, company):
    try:
        requests.adapters.DEFAULT_RETRIES = 100
        response = requests.get(url)
        data = xmltodict.parse(response.content)
        key = data["api"]["query"]["pages"]["page"]["@_idx"]
        if key == '-1':
            html_text = 'Not_found'
            return html_text
        else:
            #to check if data contains text
            text_path = data["api"]["query"]["pages"]["page"]["extract"]
            html_text = "No_text"
            if "#text" in text_path.keys():
                html_text = text_path["#text"]
            return html_text
    except requests.exceptions.ConnectionError:
        return -1

'''
this method will create the dictionary for all companies with key the company name
and value the corresponding html text
'''
def html_File(link, companies, companies_encoded):
    html_texts = {}
    for i in range(len(companies)):
        print(i)
        while(True):
            html_text = html_Company(link + companies_encoded[i], companies[i])
            if html_text != -1:
                html_texts[companies[i]] = html_text
                break
    return html_texts
            
'''
this method will save the data into the file specified (file of type json: "dictionary structure")
'''
def save_File(data, file):
    json_data = json.dumps(data)
    f = open(file, "w")
    f.write(json_data)
    f.close()
    
def load_File(file_path):
    return json.load(open(file_path))

def slice_Dictionary(dictionary, k):
    stop = 0
    dic = {}
    for key in dictionary.keys():
        dic[key] = dictionary[key]
        stop += 1
        if stop == k:
            break
    return dic
'''
the elements of l should support len
'''
def list_Group_Length(l):
    total_length = 0
    for element in l:
        total_length += len(element)
    return total_length

def extract_Abstracts(html_texts):
    abstracts = {}
    for key in html_texts.keys():
        if html_texts[key] == "Not_found":
            abstracts[key] = "Not_found"
            
        elif html_texts[key] == "No_text":
            abstracts[key] = ""
        
        else:
            end_abstract = "<span id="
            index_end = html_texts[key].find(end_abstract)
            abstract = html_texts[key][0:index_end]
            soup = BeautifulSoup(abstract, "html.parser")
            abstract = soup.get_text().replace(u'\xa0', u' ').strip("\n")
            abstracts[key] = abstract
    return abstracts
            
def extract_Headers(html_texts):
    dictionary_headers = {}
    for key in html_texts.keys():
        soup = BeautifulSoup(html_texts[key], "html.parser")
        headers = soup.find_all(re.compile(r"^h"))
        headers_string = [header.get_text().replace(u'\xa0', u' ') for header in headers]
        dictionary_headers[key] = headers_string
    return dictionary_headers

def extract_HTML_Headers(html_texts):
    dictionary_headers = {}
    for key in html_texts.keys():
        soup = BeautifulSoup(html_texts[key], "html.parser")
        headers = soup.find_all(re.compile(r"^h"))
        dictionary_headers[key] = headers
    return dictionary_headers

def extract_End_Headers(headers):
    end_headers = set()
    for key in headers.keys():
        list_headers = headers[key]
        if list_headers != [] and len(list_headers) > 3:
            end_headers.add(list_headers[len(list_headers) - 1])
    return list(end_headers)

'''
to find which elements of list1 exist in list2: can have duplicates
ex: list1 = (1,1,2,3) intersect (1,5,7) ----> (1, 1)
k will specify the limit of retrieved common elements
'''
def intersection_Lists(list1, list2, k):
    intersection = []
    stop = 0
    for element in list1:
        if k >= 1 and stop != k:
            if element in list2:
                intersection.append(element)
                stop += 1
        else:
            break
    return intersection

def restore_Intersection(list_headers, first_unimp_section):
    backup = ""
    if first_unimp_section != []:
        for i in range(len(list_headers)):
            if list_headers[i].lower().replace(u' ', u'') == first_unimp_section[0]:
                backup = list_headers[i]
                return [backup, i]
    return [backup, -1]
    
    
def uncover_Unimportant_Sections(headers, end_headers_encoded):
    unimportant_sections_restored = {}
    unimportant_sections_encoded = {}
    for key in headers.keys():
        list_headers = headers[key]
        list_headers_encoded = [header.lower().replace(u' ', u'') for header in list_headers]
        first_unimp_section = intersection_Lists(list_headers_encoded, end_headers_encoded, 1)
        [first_unimp_section_restored, index] = restore_Intersection(list_headers, first_unimp_section)
        if index != -1:
            unimportant_sections = headers[key][index:] 
            unimportant_sections_restored[key] = unimportant_sections
            unimportant_sections_encoded[key] = [x.lower().replace(u' ', u'') for x in unimportant_sections]
        else:
            unimportant_sections_restored[key] = []
            unimportant_sections_encoded[key] = []
    return [unimportant_sections_encoded, unimportant_sections_restored]
        

def remove_Unimportant_Sections(html_texts, unimportant_sections, end_headers):
    html_texts_filtered = {}
    for key in html_texts.keys():
        value = html_texts[key]
        if unimportant_sections[key] == []:
            html_texts_filtered[key] = value
        else:
            unimportant_sections_start = unimportant_sections[key][0].replace(u' ',u'_')            
            index_to_remove = value.find("<span id=\"" + unimportant_sections_start + "\"")
            text = value[0:index_to_remove]
            html_texts_filtered[key] = text
    return html_texts_filtered

        
def get_Clean_Text(html_texts_filtered):
    clean_texts = {}
    for key in html_texts_filtered.keys():
        if html_texts_filtered[key] == "Not_found":
            clean_texts[key] = "Not_found"
        elif html_texts_filtered[key] == "No_text":
            clean_texts[key] = ""
        else:
            soup = BeautifulSoup(html_texts_filtered[key], "html.parser")
            clean_text = soup.get_text().replace(u'\xa0', u' ').strip("\n")
            clean_texts[key] = clean_text
    return clean_texts

def get_Body_Text(html_texts_filtered):
    body_texts = {}
    for key in html_texts_filtered.keys():
        if html_texts_filtered[key] == "Not_found":
            body_texts[key] = "Not_found"
        elif html_texts_filtered[key] == "No_text":
            body_texts[key] = ""
        else:
            index_body = html_texts_filtered[key].find("<span id=")
            if index_body == -1:
                body_texts[key] = ""
            else:
                body_text = html_texts_filtered[key][index_body:]
                soup = BeautifulSoup(body_text, "html.parser")
                body_text = soup.get_text().replace(u'\xa0', u' ').strip("\n")
                body_texts[key] = body_text
    return body_texts

def valid_Abstract_Body(abstracts, body_texts):
    valid_abstracts = {}
    valid_body_text = {}
    for key in abstracts.keys():
        if abstracts[key] != "Not_found" and abstracts[key] != "" and body_texts[key] != "Not_found" and body_texts[key] != "":
            valid_abstracts[key] = abstracts[key]
            valid_body_text[key] = body_texts[key]
    return [valid_abstracts, valid_body_text]
            

'''
text has a dictionary structure, this method will tokenize the text into sentences
'''
def tokenize_Text(text):
    text_tokenized = {}
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    for key in text.keys():
        text_tokenized[key] = tokenizer.tokenize(text[key])
    return text_tokenized

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2., height,
                '%d' % int(height),
                ha='center', va='bottom')
  
'''
takes a dictionary and returns also a dictionary with keys representing
the different lengths' values of the dictionary given as parameter
and value the occurence of that length in the dictionary.
the structure of the dictionary should support "len" method for dictionary
value
'''
def dic_Length(dic):
    dic_lengths = {}
    for key in dic.keys():
        length = len(dic[key])
        if length in dic_lengths.keys():
            dic_lengths[length] += 1
        else:
            dic_lengths[length] = 1
    return dic_lengths

def plot(dictionary):
    list_1, list_2 = extract_Ordered_Lists(dictionary)
    rects = plt.bar(list_1, list_2, color='g')
    plt.xticks(list_1)
    autolabel(rects)
    plt.show()  
    

def encode(List):
    return list({l.lower().replace(u' ', u'') for l in List})

'''
will return a dictionary that determines the importance vs the unimportance of using
each end_header in determining the unimportant sections
'''
def important_vs_Unimportant(unimportant_sections, end_headers):
    important_vs_unimportant = {}
    for key in unimportant_sections.keys():
        unimp_headers = unimportant_sections[key]
        for (index, header) in enumerate(unimp_headers):
            number_sections = len(unimp_headers) - index
            if header in end_headers:
                if header in important_vs_unimportant.keys():
                    if number_sections > 10:
                        important_vs_unimportant[header][1] += 1
                    else:
                        important_vs_unimportant[header][0] += 1
                else:
                    important_vs_unimportant[header] = [0, 0]
                    if number_sections > 10:
                        important_vs_unimportant[header][1] = 1
                    else:
                        important_vs_unimportant[header][0] = 1  
                    
    return important_vs_unimportant 

'''
will return a dictionary that determines how many times an end_header played a role
in determining the number of unimportant sections
'''
def role_Played(unimportant_sections):
    role_played= {}
    for key in unimportant_sections.keys():
        if unimportant_sections[key] != []:
            if unimportant_sections[key][0] in role_played.keys():
                role_played[unimportant_sections[key][0]] += 1
            else:
                role_played[unimportant_sections[key][0]] = 1
    return role_played

"""
this method was used to filter a dictionary that has as key "end header" and a tuple as value and that represents two infos:
the first element in the tuple represents how many times the "end header" succeed in cutting the unimportant sections
and the second element represents how many times that "end header" failed in cutting the unimportant sections
and at the end we return a list of the headers that succeeded well depending on the info presented in the tuple
"""
def filter_Dictionary(dic):
    list_l = []
    for key in dic.keys():
        if dic[key][1] > dic[key][0]:
            continue
        elif dic[key][1] > 4:
            continue
        elif dic[key][0] < 5:
            continue
        elif dic[key][0] < 5*dic[key][1]:
            continue
        else:
            list_l.append(key)
    return list_l

def smooth_Unimportant_Sections(unimportant_sections, end_headers):
    smooth_unimportant_sections = {}
    for key in unimportant_sections.keys():
        index = 0
        if len(unimportant_sections[key]) > 7:
            while(len(unimportant_sections[key][index:]) > 7):
                for (i, header) in enumerate(unimportant_sections[key][1:]):
                    if header.lower().replace(u' ',u'') in end_headers:
                        smooth_unimportant_sections[key] = unimportant_sections[key][i:]
                        index = i
        else:
            smooth_unimportant_sections[key] = unimportant_sections[key]
    return smooth_unimportant_sections


def tokenize_Words(text):
    text_words = {}
    for key in text.keys():
        text_words[key] = nltk.word_tokenize(text[key])
    return text_words

#filter the dictionary from the keys that are included in biased companies
def final_Filter(biased_companies, dictionary):
    filter_dictionary = {}
    unbiased_companies = list(set(list(valid_abstracts.keys())).difference(set(biased_companies)))
    for unbiased_companie in unbiased_companies:
        filter_dictionary[unbiased_companie] = dictionary[unbiased_companie]
    return filter_dictionary


def create_Vocab(abstracts_tokenized_words, body_text_tokenized_words):
    vocab_counter = collections.Counter()
    for key in abstracts_tokenized_words.keys():
        tokens_abs = abstracts_tokenized_words[key]
        tokens_abs = [t.lower() for t in tokens_abs]
        tokens_art = body_text_tokenized_words[key]
        tokens_art = [t.lower() for t in tokens_art]
        vocab_counter.update(tokens_abs)
        vocab_counter.update(tokens_art)
    with open(output_dir+"/vocab/vocab.txt", 'w') as writer:
        for word, count in vocab_counter.most_common():
            writer.write(word + ' ' + str(count) + '\n')

def save_File_Txt(data, file):
    with open(file, 'w') as writer:
        for line in data:
            writer.write(line + "\n")

def extract_Ordered_Lists(dic):
    list_1 = []
    list_2 = []
    for key in dic.keys():
        if list_1 == []:
            list_1.append((int)(key))
            list_2.append(dic[key])
        else:
            check = False
            for i in range(len(list_2)):
                if (int)(key) < list_1[i]:
                    list_1.insert(i, (int)(key))
                    list_2.insert(i, dic[key])
                    check = True
                    break
            if check == False:
                list_1.insert(len(list_2), (int)(key))
                list_2.insert(len(list_2), dic[key])
    return [list_1, list_2]

def biased_Companies(abstracts_tokenized_words, body_text_tokenized_words, abstracts_tokenized, body_text_tokenized):
    biased_companies = []
    for key in abstracts_tokenized_words.keys():
        if len(body_text_tokenized_words[key]) < len(abstracts_tokenized_words[key]) or len(abstracts_tokenized[key]) > 5 or len(body_text_tokenized[key]) > 100 or len(body_text_tokenized[key]) < 6 or len(body_text_tokenized[key]) < len(abstracts_tokenized[key]):
            biased_companies.append(key)
    return biased_companies




parser = argparse.ArgumentParser()
parser.add_argument("input_file", type=str, help="path to file containing company names (text file)")
parser.add_argument("output_dir", type=str, help="path of directory to store bodies files")

args = parser.parse_args()

output_dir = args.output_dir
input_file = args.input_file


# if not os.path.exists("data"): os.makedirs("data")


if not os.path.exists(output_dir+"/abstracts"): os.makedirs(output_dir+"/abstracts")
if not os.path.exists(output_dir+"/body_text"): os.makedirs(output_dir+"/body_text")
if not os.path.exists(output_dir+"/clean_text"): os.makedirs(output_dir+"/clean_text")
if not os.path.exists(output_dir+"/companies"): os.makedirs(output_dir+"/companies")
if not os.path.exists(output_dir+"/headers"): os.makedirs(output_dir+"/headers")
if not os.path.exists(output_dir+"/html"): os.makedirs(output_dir+"/html")
if not os.path.exists(output_dir+"/unimportant_sections"): os.makedirs(output_dir+"/unimportant_sections")





link = "https://en.wikipedia.org/w/api.php?format=xml&action=query&prop=extracts&redirects&titles="

[companies, companies_encoded] = process_Companies_1(input_file)

html_texts = html_File(link, companies, companies_encoded)
save_File(html_texts, output_dir+"/html_texts.json")


html_texts = load_File(output_dir+"/html/html_texts.json")



abstracts = extract_Abstracts(html_texts)
save_File(abstracts, output_dir+"/abstracts/abstracts.json")



abstracts = load_File(output_dir+"/abstracts/abstracts.json")



headers = extract_Headers(html_texts)
save_File(headers, output_dir+"/headers/headers.json")

end_headers = extract_End_Headers(headers)
save_File(end_headers, output_dir+"/headers/end_headers.json")


headers = load_File(output_dir+"/headers/headers.json")
end_headers = load_File(output_dir+"/headers/end_headers.json")


end_headers_encoded = encode(end_headers)
[unimportant_sections_encoded, unimportant_sections_restored] = uncover_Unimportant_Sections(headers, end_headers_encoded)



important_vs_unimportant = important_vs_Unimportant(unimportant_sections_encoded, end_headers_encoded)
end_headers_filtered = filter_Dictionary(important_vs_unimportant)  
[unimportant_sections_encoded2, unimportant_sections_restored2] = uncover_Unimportant_Sections(headers, end_headers_filtered)


role_played = role_Played(unimportant_sections_encoded2)
[unimportant_sections_encoded3, unimportant_sections_restored3] = uncover_Unimportant_Sections(headers, role_played)


end_headers_chosen = role_played
save_File(end_headers_chosen, output_dir+"/headers/end_headers_chosen.json")

unimportant_sections3_smoothed = smooth_Unimportant_Sections(unimportant_sections_restored3, end_headers_chosen)
save_File(unimportant_sections3_smoothed, output_dir+"/unimportant_sections/unimportant_sections3_smoothed.json")






end_headers_chosen = load_File(output_dir+"/headers/end_headers_chosen.json")
unimportant_sections_smoothed = load_File(output_dir+"/unimportant_sections/unimportant_sections3_smoothed.json")



html_texts_filtered = remove_Unimportant_Sections(html_texts, unimportant_sections_smoothed, end_headers_chosen)
save_File(html_texts_filtered, output_dir+"/html/html_texts_filtered.json")


html_texts_filtered = load_File(output_dir+"/html/html_texts_filtered.json")


clean_texts = get_Clean_Text(html_texts_filtered)
save_File(clean_texts, output_dir+"/clean_text/clean_texts.json")


clean_texts = load_File(output_dir+"/clean_text/clean_texts.json")


body_texts = get_Body_Text(html_texts_filtered)
save_File(body_texts, output_dir+"/body_text/body_texts.json")

body_texts = load_File(output_dir+"/body_text/body_texts.json")


[valid_abstracts, valid_body_text] = valid_Abstract_Body(abstracts, body_texts)
save_File(valid_abstracts, output_dir+"/abstracts/valid_abstracts.json")
save_File(valid_body_text, output_dir+"/body_text/valid_body_text.json")



valid_abstracts = load_File(output_dir+"/abstracts/valid_abstracts.json")
valid_body_text = load_File(output_dir+"/body_text/valid_body_text.json")



valid_abstracts_tokenized = tokenize_Text(valid_abstracts)
save_File(valid_abstracts_tokenized, output_dir+"/abstracts/valid_abstracts_tokenized.json")


valid_abstracts_tokenized = load_File(output_dir+"/abstracts/valid_abstracts_tokenized.json")




valid_body_text_tokenized = tokenize_Text(valid_body_text)
save_File(valid_body_text_tokenized, output_dir+"/body_text/valid_body_text_tokenized.json")


valid_body_text_tokenized = load_File(output_dir+"/body_text/valid_body_text_tokenized.json")


valid_body_text_tokenized_words = tokenize_Words(valid_body_text)
save_File(valid_body_text_tokenized_words, output_dir+"/body_text/valid_body_text_tokenized_words.json")



valid_abstracts_tokenized_words = tokenize_Words(valid_abstracts)
save_File(valid_abstracts_tokenized_words, output_dir+"/abstracts/valid_abstracts_tokenized_words.json")


valid_abstracts_tokenized_words = load_File(output_dir+"/abstracts/valid_abstracts_tokenized_words.json")
valid_body_text_tokenized_words = load_File(output_dir+"/body_text/valid_body_text_tokenized_words.json")




biased_companies = biased_Companies(valid_abstracts_tokenized_words, valid_body_text_tokenized_words, valid_abstracts_tokenized, valid_body_text_tokenized)
save_File_Txt(biased_companies, output_dir+"/companies/biased_companies.txt")

biased_companies, _ = process_Companies_1(output_dir+"/companies/biased_companies.txt")


save_File(final_Filter(biased_companies, valid_abstracts), output_dir+"/abstracts/filtered_valid_abstracts.json")
save_File(final_Filter(biased_companies, valid_abstracts_tokenized_words), output_dir+"/abstracts/filtered_valid_abstracts_tokenized_words.json")
save_File(final_Filter(biased_companies, valid_abstracts_tokenized), output_dir+"/abstracts/filtered_valid_abstracts_tokenized.json")

save_File(final_Filter(biased_companies, valid_body_text), output_dir+"/body_text/filtered_valid_body_text.json")
save_File(final_Filter(biased_companies, valid_body_text_tokenized_words), output_dir+"/body_text/filtered_valid_body_text_tokenized_words.json")
save_File(final_Filter(biased_companies, valid_body_text_tokenized), output_dir+"/body_text/filtered_valid_body_text_tokenized.json")


save_File_Txt(list(filtered_valid_abstracts.keys()), output_dir+"/companies/companies_filtered.txt")

