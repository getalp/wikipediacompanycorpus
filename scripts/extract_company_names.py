import mwxml
import re
import argparse



def extract_company_names(dump_filename):


	# dump_filename = '/home/qader/Downloads/enwiki-20170820-pages-articles.xml'

	dump = mwxml.Dump.from_file(dump_filename)

	for page_i, page in enumerate(dump):
		
		for revision in page:


			try:

				# # query: to extract copmany titles based on infobox
				# if re.match('{{Infobox company[\s\S]*?}}', revision.text):
					# print(page.title)


				# query: to extract copmany titles based on category
				if re.search(r'\[\[Category:(.+)\]\]', revision.text):
					categories = re.findall('\[\[Category:(.+)\]\]', revision.text)
					categories = [cat for cat in categories]
					categories_s = ' '.join(categories)
					if 'company' in categories_s or 'companies' in categories_s:
						print(page.title)



			except: pass






if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dump_file', type=str, help='path to wikipedia dump file')

    args = parser.parse_args()

    extract_company_names(args.dump_file)
