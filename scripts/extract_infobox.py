# extracts infoboxes from wikipedia pages
# infobox1 is the text version of the infobox attributes
# infobox2 is the listed version of the infobox attirbutes



import subprocess
import os, sys
from bs4 import BeautifulSoup
import urllib.request
import json
from urllib.parse import quote
import requests
import argparse



def extract_infobox(company_names_path, infobox1_path, infobox2_path):

	# load company names
	companies = []
	with open(company_names_path, 'r', encoding = 'utf-8') as fin:
		for line in fin:
			line = line.strip()
			companies.append(line)



	infobox1 = {}
	infobox2 = {}

	for c_i, company in enumerate(companies):
		print(c_i)

		try:
			site= "http://en.wikipedia.org/wiki/" + quote(company)
			hdr = {'User-Agent': 'Mozilla/5.0'}
			req = urllib.request.Request(site,headers=hdr)
			page = urllib.request.urlopen(req)
			soup = BeautifulSoup(page.read(), "lxml")
			table = soup.find('table', class_='infobox')
			if table != None:
				result_infobox1 = {}
				result_infobox2 = {}
				for tr in table.find_all('tr'):
					if tr.find('th') == None:
						continue
					elif tr.find('td') == None:
						continue
					else:
						result_infobox1[tr.find('th').text] = tr.find('td').text
						result_infobox2[tr.find('th').text] = [str(t) for t in tr.find('td').findAll(text=True)]

				infobox1[company] = result_infobox1
				infobox2[company] = result_infobox2


		except:
			continue

	with open(infobox1_path, 'w') as fh:
		fh.write(json.dumps(infobox1))


	with open(infobox2_path, 'w') as fh:
		fh.write(json.dumps(infobox2))






if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("input_file", type=str, help="path to file containing company names (.json)")
	parser.add_argument("infobox1", type=str, help="path to store infobox1 (.json)")
	parser.add_argument("infobox2", type=str, help="path to store infobox2 (.json)")

	args = parser.parse_args()

	extract_infobox(args.input_file, args.infobox1, args.infobox2)
