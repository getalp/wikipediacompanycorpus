# Removes company names with two or less attributes

import subprocess
import os, sys
from bs4 import BeautifulSoup
import urllib.request
import json
from urllib.parse import quote
import requests
import argparse



def filter_companies(input_file, output_file):


	company_related_features = set(['Headquarters','Founded','Industry','Type','Key people','Products','Number of employees','Founder','Area served','Owner','Defunct','Services','Number of locations','Fate','Former type','Founders','Formerly called','Brands','Areas served','Owners','Employees','Type of business', 'Production output', 'Revenue', 'Net income', 'Service area', 'Service type', 'Parent', 'Members', 'Dates of operation', 'Predecessor', 'Successor', 'Product type', 'Related brands'])


	# load company names
	companies = []
	with open(input_file, 'r', encoding = 'utf-8') as fin:
		for line in fin:
			line = line.strip()
			companies.append(line)



	mask = open(output_file, "w") 

	for c_i, company in enumerate(companies):
		print(c_i)

		try:
			site= "http://en.wikipedia.org/wiki/" + quote(company)
			hdr = {'User-Agent': 'Mozilla/5.0'}
			req = urllib.request.Request(site,headers=hdr)
			page = urllib.request.urlopen(req)
			soup = BeautifulSoup(page.read(), "lxml")
			table = soup.find('table', class_='infobox')
			if table != None:
				result = {}
				for tr in table.find_all('tr'):
					if tr.find('th') == None:
						continue
					elif tr.find('td') == None:
						continue
					else:
						result[tr.find('th').text] = tr.find('td').text
				cur_comp_attr = set([key.strip().strip('\n') for key in result.keys()])
				inters = set.intersection(cur_comp_attr, company_related_features)

				mask.write('TWO_AND_MORE: ' + company + "\n")


		except:
			continue



	mask.close()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", type=str, help="path to file containing company names (text file)")
    parser.add_argument("output_file", type=str, help="path to store the output (text file)")
    args = parser.parse_args()

    filter_companies(args.input_file, args.output_file)