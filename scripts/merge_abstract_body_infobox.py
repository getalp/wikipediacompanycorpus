
import json
import argparse
from collections import *



def merge_abstract_infobox_body(abstract_file_path, infobox1_file_path, infobox2_file_path, body_file_path, output_file_path):

	abstracts = json.load(open(abstract_file_path))
	infobox1 = json.load(open(infobox1_file_path))
	infobox2 = json.load(open(infobox2_file_path))
	body_texts = json.load(open(body_file_path))

	abstract_infobox_body = defaultdict(dict)


	for key in abstracts:

		print(key)
		
		if key in infobox1 and key in infobox2 and key in body_texts:
			abstract_infobox_body[key]['infobox1'] = infobox1[key]
			abstract_infobox_body[key]['infobox2'] = infobox2[key]
			abstract_infobox_body[key]['abstract'] = abstracts[key]
			abstract_infobox_body[key]['body'] = body_texts[key]


	with open(output_file_path, 'w') as fout:
		fout.write(json.dumps(abstract_infobox_body))



if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("abstract_file", type=str, help="path to file containing abstracts (.json)")
	parser.add_argument("infobox1", type=str, help="path to file containing infobox1 (.json)")
	parser.add_argument("infobox2", type=str, help="path to file containing infobox2 (.json)")
	parser.add_argument("body", type=str, help="path to file containing body file (.json)")
	parser.add_argument("output", type=str, help="path to store the merged file (.json)")


	args = parser.parse_args()

	merge_abstract_infobox_body(args.abstract_file, args.infobox1, args.infobox2, args.body, args.output)
